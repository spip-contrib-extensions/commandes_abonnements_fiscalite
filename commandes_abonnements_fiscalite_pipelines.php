<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajouter des champs aux offres d'abonnements pour configurer des infos de déduction fiscale
 * 
 * @pipeline declarer_tables_objets_sql
 * @param array $flux
 * 		Flux du pipeline contenant toutes les tables et leurs infos
 * @return array
 * 		Retourne le flux possiblement modifié
 **/
function commandes_abonnements_fiscalite_declarer_tables_objets_sql($flux) {
	$flux['spip_abonnements_offres']['field']['fiscalite_type'] = 'varchar(50) not null default ""';
	$flux['spip_abonnements_offres']['field']['fiscalite_montant_deduit'] = 'decimal(20,6) not null default 0';
	$flux['spip_abonnements_offres']['champs_editables'][] = 'fiscalite_type';
	$flux['spip_abonnements_offres']['champs_editables'][] = 'fiscalite_montant_deduit';
	$flux['spip_abonnements_offres']['champs_versionnes'][] = 'fiscalite_type';
	$flux['spip_abonnements_offres']['champs_versionnes'][] = 'fiscalite_montant_deduit';
	
	return $flux;
}

/**
 * Modifier les saisies d'édition d'une offre pour ajouter les nouveaux champs
 * 
 * @pipeline formulaire_saisies
 * @param array $flux
 * 		Flux du pipeline contenant toutes les saisies des formulaires
 * @return array
 * 		Retourne le flux possiblement modifié
 **/
function commandes_abonnements_fiscalite_formulaire_saisies($flux) {
	// Si on est dans le formulaire d'édition d'une offre
	if ($flux['args']['form'] == 'editer_abonnements_offre') {	
		include_spip('inc/saisies');
		
		$flux['data'] = saisies_inserer(
			$flux['data'],
			array(
				'saisie' => 'radio',
				'options' => array(
					'nom' => 'fiscalite_type',
					'label' => _T('abonnementsoffre:champ_fiscalite_type_label'),
					'data' => array(
						'' => _T('abonnementsoffre:champ_fiscalite_type_choix__label'),
						'fr_personne' => _T('abonnementsoffre:champ_fiscalite_type_choix_fr_personne_label'),
						'fr_entreprise' => _T('abonnementsoffre:champ_fiscalite_type_choix_fr_entreprise_label'),
					),
				),
			)
		);
		$flux['data'] = saisies_inserer(
			$flux['data'],
			array(
				'saisie' => 'input',
				'options' => array(
					'nom' => 'fiscalite_montant_deduit',
					'label' => _T('abonnementsoffre:champ_fiscalite_montant_deduit_label'),
					'afficher_si' => '@fiscalite_type@ != ""',
				),
				'verifier' => array(
					'type' => 'decimal',
					'options' => array(
						'min' => 0,
					),
				),
			)
		);
	}
	
	return $flux;
}

/**
 * Modifier le résultat de la compilation d'un squelette
 *
 * => Ajouter les nouveaux champs dans la fiche d'une offre d'abonnement
 *
 * @pipeline recuperer_fond
 * @param array $flux
 * 		Flux du pipeline contenant le squelette compilé
 * @return array
 * 		Retourne le flux possiblement modifié
 */
function commandes_abonnements_fiscalite_recuperer_fond($flux) {
	if (
		isset($flux['args']['fond'])
		and $flux['args']['fond'] == 'prive/objets/contenu/abonnements_offre'
		and isset($flux['args']['contexte'])
		and $complement = recuperer_fond('prive/objets/contenu/abonnements_offre_complement_fiscalite', $flux['args']['contexte'])
	) {
		$flux['data']['texte'] .= $complement;
	}
	elseif ($flux['args']['fond'] == 'formulaires/inc-resume_offre') {
		$offre = sql_fetsel('fiscalite_type, fiscalite_montant_deduit', 'spip_abonnements_offres', 'id_abonnements_offre = '.intval($flux['args']['contexte']['id_abonnements_offre']));
		
		if ($offre['fiscalite_type']) {
			$tous_les_taux = commandes_abonnements_fiscalite_taux();
			$taux = $tous_les_taux[$offre['fiscalite_type']];
			$ajout = '<p class="abonnement__fiscalite" data-fiscalite-taux="'.$taux.'" data-fiscalite-montant-deduit="'.$offre['fiscalite_montant_deduit'].'">'._T('abonnementsoffre:fiscalite_cout', array('montant' => montant_formater(0)), array('sanitize'=>false)).'</p>';
			$flux['data']['texte'] = str_replace('<footer class="abonnement__footer">', '<footer class="abonnement__footer">'.$ajout, $flux['data']['texte']);
		}
	}
	elseif ($flux['args']['fond'] == 'formulaires/commander_abonnement') {
		$javascript = <<<JS
<script type="text/javascript">
/*<![CDATA[*/
;(function($){
	function commandes_abonnements_fiscalite_chercher_prix_final(radios, libre) {
		var prix = 0;
		
		// Si le prix libre est rempli
		if (prix = libre.val()) {
			prix = prix;
		}
		else {
			prix = radios.filter(':checked').val();
		}
		prix = parseFloat(prix);
		
		return prix;
	}
	
	$(function(){
		$('[data-fiscalite-taux]').each(function(){
			var me = $(this);
			var taux = me.data('fiscalite-taux');
			var montant_deduit = me.data('fiscalite-montant-deduit');
			var radios = me.parents('.abonnement__inner').find('input[type=radio]');
			var libre = me.parents('.abonnement__inner').find('input[type=text]');
			var devise = me.find('.montant__devise')[0].outerHTML;
			var prix_deduction = 0;
			
			// On cherche le prix choisi à payer au démarrage
			var prix = commandes_abonnements_fiscalite_chercher_prix_final(radios, libre);
			prix_deduction = prix - (prix - montant_deduit) * taux;
			me.find('.montant').html(prix_deduction.toFixed(2) + ' ' + devise);
			
			// À chaque changement on recalcule
			radios.change(function() {
				prix = commandes_abonnements_fiscalite_chercher_prix_final(radios, libre);
				prix_deduction = prix - (prix - montant_deduit) * taux;
				me.find('.montant').html(prix_deduction.toFixed(2) + ' ' + devise);
			});
			libre.on('input propertychange paste', function() {
				prix = commandes_abonnements_fiscalite_chercher_prix_final(radios, libre);
				prix_deduction = prix - (prix - montant_deduit) * taux;
				me.find('.montant').html(prix_deduction.toFixed(2) + ' ' + devise);
			});
		});
	});
})(jQuery);
/*]]>*/
</script>
JS;
		$flux['data']['texte'] .= $javascript;
	}
	
	return $flux;
}

function commandes_abonnements_fiscalite_taux() {
	return array(
		'fr_personne' => 0.66,
		'fr_entreprise' => 0.6,
	);
}
