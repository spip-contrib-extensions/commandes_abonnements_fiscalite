<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/commandes_abonnements.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'champ_fiscalite_type_label' => 'Déduction fiscale applicable',
	'champ_fiscalite_type_choix__label' => 'Aucune déduction',
	'champ_fiscalite_type_choix_fr_personne_label' => 'Déduction pour les personnes (66%)',
	'champ_fiscalite_type_choix_fr_entreprise_label' => 'Déduction pour les entreprises (60%)',
	'champ_fiscalite_montant_deduit_label' => 'Montant non pris en compte dans la déduction',
	
	// F
	'fiscalite_cout' => 'Après réduction fiscale, le coût est de <span class="abonnement__fiscalite_cout">@montant@</span>',
);
